import PropTypes from "prop-types";
import classnames from "classnames";

import plusIcon from "../../assets/plus-icon.svg";
import minIcon from "../../assets/minus-icon.svg";

import style from "./Todos.module.css";

const Todos = ({ todos, onSubstraction, onAddition }) => {
  return (
    <div className={style.todos}>
      {todos.map((todo, index, arr) => {
        return (
          <div
            key={index}
            className={classnames(style.todo, {
              [style.todoDivider]: !(arr.length === index + 1),
            })}
          >
            {todo.title}
            <div className={style.todoIconWrapper}>
              <div className={style.todoCount}>{todo.count}</div>
              <button
                onClick={() => onSubstraction(index)}
                className={style.todoActionButton}
              >
                <img src={minIcon} alt="minus icon" />
              </button>
              <button
                onClick={() => onAddition(index)}
                className={style.todoActionButton}
              >
                <img src={plusIcon} alt="plus icon" />
              </button>
            </div>
          </div>
        );
      })}
    </div>
  );
};

Todos.propTypes = {
  todos: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      count: PropTypes.number,
    })
  ),
  onSubstraction: PropTypes.func,
  onAddition: PropTypes.func,
};

export default Todos;
